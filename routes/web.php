<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', 'RegistrationController@create');
Route::post('register', 'RegistrationController@store');

Route::get('/tournament', 'RegistrationController@createTournament');
Route::post('tournament', 'RegistrationController@storeTournament');

Route::get('viewTournament', 'TournamentController@viewTournament');
Route::post('setTeams', 'SetTeamsController@setTeams');
Route::post('gamePlay', 'GamePlayController@gamePlay');

Route::get('deleteTournament/{id}','TournamentController@deleteTournament');
Route::get('stopTournament/{id}','TournamentController@stopTournament');
Route::get('startTournament/{id}','TournamentController@startTournament');

Route::get('/viewScore', 'ScoreController@viewScore');
Route::post('storeScore', 'ScoreController@storeScore');

Route::get('score','ScoreController@updateFinalScores');
Route::get('/setWinner', 'ScoreController@setWinner');
