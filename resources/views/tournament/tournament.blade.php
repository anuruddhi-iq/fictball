@extends('layouts.master')
@section('content')
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

    <nav class = "navbar navbar-expand-sm bg-dark navbar-dark mt-5">
        <a href = "" class = "navbar-brand">FICTBALL</a>

        <div class = "collapse navbar-collapse justify-content-center" id ="ulist">

            <ul class = "navbar-nav">

                <li class = "nav-item " ><a href = "/tournament" class = "nav-link">Tournament Registration</a></li>
                <li class = "nav-item "><a href = "/register" class = "nav-link">Team Registration</a></li>
                <li class = "nav-item active" ><a href = "/viewTournament" class = "nav-link">View Tournaments</a></li>
                <li class = "nav-item " ><a href = "/viewScore" class = "nav-link">Scores</a></li>

            </ul>

        </div>
    </nav>
    <hr>
    <h2>Tournaments</h2>

            <table border = 3 style="width:100%;height:20%">
                <tr class="bg-secondary">
                    <td class="text-center">Tournament ID</td>
                    <td class="text-center">Tournament Name</td>
                    <td class="text-center"></td>
                </tr>
                {{$r = 1}}
                @foreach ($getTournamentDetails as $getTournamentDetail)
                <tr>
                    <td class="text-center">{{ $getTournamentDetail->id }}<input type='hidden' id='{{$r}}.tid' value='{{ $getTournamentDetail->id }}'  style='width:50px' disabled></td>
                    <td class="text-center">{{ $getTournamentDetail->display_name }}</td>
                    <td>
                        @if($getTournamentDetail->current_status == 1)
                            <a href = 'startTournament/{{ $getTournamentDetail->id }}' class="btn btn-primary btn-md disabled" role="button" style="font-size : 15px; width: 23%; height: 30px;">Start</a>
                        @else
                            <a href = 'startTournament/{{ $getTournamentDetail->id }}' class="btn btn-primary btn-md " role="button" style="font-size : 15px; width: 23%; height: 30px;" >Start</a>
                        @endif

                            @if($getTournamentDetail->current_status == 0)
                                <a href = 'stopTournament/{{ $getTournamentDetail->id }}' class="btn btn-secondary btn-md disabled" role="button" style="font-size : 15px; width: 23%; height: 30px;">Stop</a>
                            @else
                                <a href = 'stopTournament/{{ $getTournamentDetail->id }}' class="btn btn-secondary btn-md" role="button" style="font-size : 15px; width: 23%; height: 30px;">Stop </a>
                            @endif

                        <a href = 'deleteTournament/{{ $getTournamentDetail->id }}' class="btn btn-danger btn-md" style="font-size : 15px; width: 23%; height: 30px;" role="button">Delete</a>
                        <button type="button" class="btn btn-success btn-md" id="set_matches" onclick="myFunction()" style="font-size : 15px; width: 23%; height: 30px;">Teams</button>
                            <div id="team_structure"></div>


                    </td>
                </tr>
                    {{$r++}}
                @endforeach
            </table>

<div class="container">
    <!-- Trigger the modal with a button -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                    <h4 class="modal-title" id="header_name"></h4>
                </div>
                <div class="modal-body" id="body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" onclick="calculate_score();">End & Submit </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

</div>

<script>
    $.ajaxSetup({
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        },
    });

    function myFunction() {
        $.ajax({
            type: 'POST',
            data: {type:'setTeams'},
            url: 'setTeams',
            success: function (data) {
                $('#team_structure').empty().append(data);
            },
            error: function (data) {
            }
        });
    }

    function update_val(data){
        var bonus = 0;
        var old_val =   parseInt(document.getElementById(data).value);
        var bonus_status =   parseInt(document.getElementById('bonus_status').value);
        if(bonus_status == 0){

            if((data=='no_of_tries_1') && (old_val +1 == 3)){
                document.getElementById('bonus_point1').value = 1;
                document.getElementById('bonus_status').value = 1;

            }
            if((data=='no_of_tries_2') && (old_val +1 == 3)){
                document.getElementById('bonus_point2').value = 1;
                document.getElementById('bonus_status').value = 1;
            }
        }

        document.getElementById(data).value = old_val + 1;
    }
</script>


@endsection