@extends('layouts.master')
@section('content')
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

    <nav class = "navbar navbar-expand-sm bg-dark navbar-dark mt-5">
        <a href = "" class = "navbar-brand">FICTBALL</a>

        <div class = "collapse navbar-collapse justify-content-center" id ="ulist">

            <ul class = "navbar-nav">

                <li class = "nav-item " ><a href = "/tournament" class = "nav-link">Tournament Registration</a></li>
                <li class = "nav-item "><a href = "/register" class = "nav-link">Team Registration</a></li>
                <li class = "nav-item " ><a href = "/viewTournament" class = "nav-link">View Tournaments</a></li>
                <li class = "nav-item active" ><a href = "" class = "nav-link">Scores</a></li>

            </ul>

        </div>
    </nav>
    <hr>
    <h2>Score Board</h2>

<a href = '/setWinner' class="btn btn-secondary btn-md" style="font-size : 15px; width: 23%; height: 30px;" role="button">Set Winner</a>
<hr>
            <table border = 3 style="width:100%;height:20%">
                <tr class="bg-secondary">
                    <td class="text-center">Tournament Name</td>
                    <td class="text-center">Team Name</td>
                    <td class="text-center">Try</td>
                    <td class="text-center">Conversion</td>
                    <td class="text-center">Bonus</td>
                    <td class="text-center">Total Score</td>
                    <td class="text-center">Winning Count</td>
                    <td class="text-center">Status</td>

                </tr>
                @foreach ($viewScore as $score)
                <tr>
                    <td class="text-center">{{ $score->tournament }}</td>
                    <td class="text-center">{{ $score->teamName }}</td>
                    <td class="text-center">{{ $score->try }}</td>
                    <td class="text-center">{{ $score->conversion }}</td>
                    <td class="text-center">{{ $score->bonus }}</td>
                    <td class="text-center">{{ $score->total_score }}</td>
                    <td class="text-center">{{ $score->win_count }}</td>
                    @if ($score->status == 1)
                    <td class="text-center">Won</td>
                    @else
                        <td class="text-center">Lost</td>
                    @endif

                </tr>
                @endforeach
            </table>
@endsection

{{--<script>--}}
    {{--$.ajaxSetup({--}}
        {{--beforeSend: function(xhr, type) {--}}
            {{--if (!type.crossDomain) {--}}
                {{--xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));--}}
            {{--}--}}
        {{--},--}}
    {{--});--}}

    {{--function set_winner(){--}}
        {{--$.ajax({--}}
            {{--type: 'POST',--}}
            {{--data: {status:'set_winner'},--}}
            {{--dataType: "json",--}}
            {{--url: 'setWinner',--}}
            {{--success: function (data) {--}}

                {{--$('#header_name').empty().append(data.header_name);--}}
                {{--$('#body').empty().append(data.table);--}}
                {{--$('#myModal').modal('show');--}}

            {{--},--}}
            {{--error: function (data) {--}}
            {{--}--}}
        {{--});--}}
    {{--}--}}
{{--</script>--}}