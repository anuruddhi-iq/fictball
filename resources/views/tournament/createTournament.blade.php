@extends('layouts.master')

@section('content')
    <head><meta name="csrf-token" content="{{ csrf_token() }}"></head>

    <nav class = "navbar navbar-expand-sm bg-dark navbar-dark mt-5">
        <a href = "" class = "navbar-brand">FICTBALL</a>

        <div class = "collapse navbar-collapse justify-content-center" id ="ulist">

            <ul class = "navbar-nav">

                <li class = "nav-item active" ><a href = "/tournament" class = "nav-link">Tournament Registration</a></li>
                <li class = "nav-item "><a href = "/register" class = "nav-link">Team Registration</a></li>
                <li class = "nav-item " ><a href = "/viewTournament" class = "nav-link">View Tournaments</a></li>


            </ul>

        </div>
    </nav>
    <hr>
    <h2>Tournament Registration</h2>
    <form id="myform">

        <div class="form-group">
            <label for="name">Display Name:</label>
            <input type="text" class="form-control" id="tournament" name="tournament">
        </div>

        <div class="form-group">
            <button style="cursor:pointer" type="button" class="btn btn-secondary" onclick="register();">Submit</button>
        </div>
        <div class="form-group" id="success_msg">

        </div>
    </form>
    <script>
        $.ajaxSetup({
            beforeSend: function(xhr, type) {
                if (!type.crossDomain) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                }
            },
        });

        function register() {
            var data = $('#tournament').val();
            $('#tournament').val('');
            $.ajax({
                type: 'POST',
                data:{'tournament':data},
                url: '/tournament',
                success: function (data) {
                    $('#success_msg').empty().append(data['message']);
                    $('#tournament').empty();

                },
                error: function (data) {
                    console.log(data);
                }

            });
        }
    </script>
@endsection