@extends('layouts.master')
@section('content')

    <head><meta name="csrf-token" content="{{ csrf_token() }}"></head>
    <nav class = "navbar navbar-expand-sm bg-dark navbar-dark mt-5">
        <a href = "" class = "navbar-brand">FICTBALL</a>

        <div class = "collapse navbar-collapse justify-content-center" id ="ulist">

            <ul class = "navbar-nav">
                <li class = "nav-item " ><a href = "/tournament" class = "nav-link">Tournament Registration</a></li>
                <li class = "nav-item active"><a href = "/register" class = "nav-link">Team Registration</a></li>
                <li class = "nav-item " ><a href = "/viewTournament" class = "nav-link">View Tournaments</a></li>

            </ul>

        </div>
    </nav>
    <hr>
    <h2>Team Registration</h2>
    <form id="myform">

        <div class="form-group">
            <label for="name">Tournament:</label>
            <select class ="form-control" name="tournament" id="tournament">
                <option value="" disabled selected>Select the tournament</option>
                @foreach ($getTournamentDetails as $getTournamentDetail)
                <option value="{{ $getTournamentDetail->id }}">{{ $getTournamentDetail->display_name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="name">Display Name:</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>

        <div class="form-group">
            <button style="cursor:pointer" type="button" class="btn btn-secondary" onclick="register();">Submit</button>
        </div>

        <div class="form-group" id="success_msg"></div>
        <div class="form-group" id="error_msg"></div>

    </form>

    <script>
        $.ajaxSetup({
            beforeSend: function(xhr, type) {
                if (!type.crossDomain) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                }
            },
        });

        function register() {
            var data = $("form").serializeArray();
            $("form")[0].reset();
            $.ajax({
                type: 'POST',
                data:{'form':data},
                url: '/register',

                success: function (data) {
                    $('#success_msg').empty().append(data['message']);
                }

            });
        }
    </script>


@endsection