<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTournamentDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tournament_id')->unsigned()->nullable();
            $table->integer('match_id')->unsigned()->nullable();
            $table->integer('team_one_id')->unsigned()->nullable();
            $table->integer('team_two_id')->unsigned()->nullable();
            $table->integer('team_one_try')->unsigned()->nullable();
            $table->integer('team_two_try')->unsigned()->nullable();
            $table->integer('team_one_conversion')->unsigned()->nullable();
            $table->integer('team_two_conversion')->unsigned()->nullable();
            $table->integer('team_one_bonus')->unsigned()->nullable();
            $table->integer('team_two_bonus')->unsigned()->nullable();
            $table->integer('team_one_total')->unsigned()->nullable();
            $table->integer('team_two_total')->unsigned()->nullable();
            $table->tinyInteger('team_one_status')->nullable()->comment('0-lost|1-won');
            $table->tinyInteger('team_two_status')->nullable()->comment('0-lost|1-won');
            $table->tinyInteger('status')->nullable()->comment('0-stop|1-start');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_details');
    }
}
