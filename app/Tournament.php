<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Tournament extends Authenticatable
{
    use Notifiable;
    const START_TOURNAMENT = 1;
    const STOP_TOURNAMENT = 0;
    const ACTIVE_TOURNAMENTS = 1;
    const DEACTIVATE_TOURNAMENTS = 0;



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'display_name', 'status','logo','created_at','updated_at','deleted_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];


}