<?php

namespace App\Http\helpers;

class UtilityHelper
{
    // Response codes
    const SUCCESS = 'SUCCESS';
    const FAIL = 'FAIL';
    const VALIDATION_FAILED = 'VALIDATION_FAILED';

    /**
     * Prepare common response JSON
     * @param {string} code - Error code
     * @param {string} attribute - Attribute name. This should available only for validation errors
     * @param {object} data - Any other date to be sent. Ex:view record data
     * @param {string} message - Optional message to be sent
     * @param {int} status - Status code
     * @return {json}
     */
    static function commonResponse($code, $data = '', $message = '', $status = 200)
    {
        $msg = array(
            'code' => $code,
            'message' => $message,
            'data' => $data
        );

        return response()->json($msg, $status);
    }


}
