<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use App\Tournament;
use App\Http\helpers\UtilityHelper;
use App\Http\Controllers\Controller;
Use Redirect;

class TournamentController extends Controller
{
    public function viewTournament()
    {
        $getTournamentDetails = Tournament::where(['status'=> Tournament::ACTIVE_TOURNAMENTS])->get();
        return view('tournament.tournament')->with('getTournamentDetails', $getTournamentDetails);;
    }



    public function deleteTournament($id) {
        $getTournamentDetails = Tournament::where(['id'=> $id],['status'=> Tournament::ACTIVE_TOURNAMENTS])->get();

        if (!empty($getTournamentDetails)) {

                $updateTournamentDetails = Tournament::where(['id'=> $id])->update(['status' => Tournament::DEACTIVATE_TOURNAMENTS]);

            return Redirect::to(getenv('FRONT_URL').'viewTournament');
        }
    }


    public function stopTournament($id) {
        $getTournamentDetails = Tournament::where(['id'=> $id],['status'=> Tournament::ACTIVE_TOURNAMENTS])->get();

        if (!empty($getTournamentDetails)) {

            $updateTournamentDetails = Tournament::where(['id'=> $id])->update(['current_status' => Tournament::STOP_TOURNAMENT]);

            return Redirect::to(getenv('FRONT_URL').'viewTournament');
        }
    }


    public function startTournament($id) {
        $getTournamentDetails = Tournament::where(['id'=> $id],['status'=> Tournament::ACTIVE_TOURNAMENTS])->get();

        if (!empty($getTournamentDetails)) {

            $updateTournamentDetails = Tournament::where(['id'=> $id])->update(['current_status' => Tournament::START_TOURNAMENT]);

            return Redirect::to(getenv('FRONT_URL').'viewTournament');
        }
    }

}
?>