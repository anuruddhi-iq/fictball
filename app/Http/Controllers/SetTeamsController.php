<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use App\Tournament;
use App\TournamentDetails;
use App\Http\helpers\UtilityHelper;
use App\Http\Controllers\Controller;
use resources\views\tournament\modal_layout;


class SetTeamsController extends Controller
{
    public function setTeams()
    {
        if($_POST['type'] == 'setTeams'){

            $total =array();
        $setData = array();


            $getData = Registration::get();

            foreach($getData as $data){
                array_push($setData,$data->id);
                $total[] = array('ID' => $data->id, 'DISPLAY_NAME' => $data->display_name, 'TOURNAMENT_ID' => $data->tournament_id);
            }


            $rounds = $this->roundRobin($setData);

            $table = '<br>';
            $table .= "<table class='table table-hover table-striped' id='game_plan'>\n";
            $j = 1;
            foreach($rounds as $round => $games){

                foreach($games as $play){

                    if(($play["TEAM1"]!="ignore") && ($play["TEAM2"]!='ignore') ){

                        for($i=0; $i<5; $i++){
                            if($total[$i]['ID'] == $play["TEAM1"]){
                                $dispName1 = $total[$i]['DISPLAY_NAME'];
                                $teamId1 = $total[$i]['ID'];
                                $tournamentId = $total[$i]['TOURNAMENT_ID'];
                            }
                            if($total[$i]['ID'] == $play["TEAM2"]){
                                $dispName2 = $total[$i]['DISPLAY_NAME'];
                                $teamId2 = $total[$i]['ID'];
                            }
                            $btn_value = $play["TEAM1"].',' . $play["TEAM2"];
                        }
                        $match_id = $j;
                        $tid = 1 ;

                        $table .= "<tr><td style='width: 30%' id='$j.team1'>".$dispName1."</td>";
                        $table .= "<td style='width: 10%'>-vs-</td>";
                        $table .= "<td style='width: 30%' id='$j.team2'>".$dispName2."</td>";
                        $table .= "<td style='width: 30%'><input type='button' id='$j' value='Click To Start!' onclick='match_status($btn_value, $match_id,$tid);'></td>";
                        $table .= "<td><input type='text' id='$j._match_status' value='Pending' disabled></td></tr>";
                        $table .= "\n";

                        $j++;
                    }

                }
            }
            $table .= "</table>\n";

            return $table;


        }

    }

    public function roundRobin( array $teams ){

        if (count($teams)%2 != 0){
            array_push($teams,"ignore");
        }
        $away = array_splice($teams,(count($teams)/2));
        $home = $teams;
        for ($i=0; $i < count($home)+count($away)-1; $i++)
        {
            for ($j=0; $j<count($home); $j++)
            {
                $round[$i][$j]["TEAM1"]=$home[$j];
                $round[$i][$j]["TEAM2"]=$away[$j];
            }
            if(count($home)+count($away)-1 > 2)
            {
                $s = array_splice( $home, 1, 1 );
                $slice = array_shift( $s  );
                array_unshift($away,$slice );
                array_push( $home, array_pop($away ) );
            }
        }
        return $round;
    }

}
?>

<script>
    $.ajaxSetup({
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        },
    });


    function match_status($team_id1,$team_id2,$match_id,$tid){
        var data1 = $team_id1;
        var data2 = $team_id2;
        var match_id = $match_id;
        var tid = $tid;

        $.ajax({
            type: 'POST',
            data: {teamId1:data1, teamId2:data2 , match_id:match_id, tid:tid },
            dataType: "json",
            url: 'gamePlay',
            success: function (data) {

                $('#header_name').empty().append(data.header_name);
                $('#body').empty().append(data.table);
                $('#myModal').modal('show');

            },
            error: function (data) {
            }
        });
    }

    function calculate_score(){

        var team1_try = $('#no_of_tries_1').val();
        var team2_try = $('#no_of_tries_2').val();
        var team1_conv = $('#no_of_conv_1').val();
        var team2_conv = $('#no_of_conv_2').val();
        var bonus_team1 = $('#bonus_point1').val();
        var bonus_team2 = $('#bonus_point2').val();
        var team_id1 = $('#teamid1').val();
        var team_id2 = $('#teamid2').val();
        var match_id = $('#match_id').val();
        var tid = $('#tid').val();

        $.ajax({
            type: 'POST',
            data: {team1_try:team1_try, team2_try:team2_try, team1_conv:team1_conv, team2_conv:team2_conv, bonus_team1:bonus_team1, bonus_team2:bonus_team2, team_id1:team_id1, team_id2:team_id2, match_id:match_id, tid:tid  },
            dataType: "json",
            url: 'storeScore',
            success: function (data) {

                var x = 'Match Ended';
                document.getElementById(match_id).value = x;
                document.getElementById(match_id).disabled = true;

                if(data.team_one_status == 1){

                    var winner = document.getElementById("game_plan").rows[match_id-1].cells[0].innerHTML;

                    document.getElementById(match_id+"._match_status").value = 'Winner is '+winner;
                }
                if(data.team_two_status == 1){
                    var winner = document.getElementById("game_plan").rows[match_id-1].cells[2].innerHTML;
                    document.getElementById(match_id+"._match_status").value = 'Winner is '+winner;
                }


            },
            error: function (data) {
                alert('jjj000');
            }
        });
    }

    function set_winner(){
alert('fff');
        $.ajax({
            type: 'POST',
            data: {status:'set_winner'},
            dataType: "json",
            url: 'gamePlay',
            success: function (data) {

                $('#header_name').empty().append(data.header_name);
                $('#body').empty().append(data.table);
                $('#myModal').modal('show');

            },
            error: function (data) {
            }
        });
    }
</script>
