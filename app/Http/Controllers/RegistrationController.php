<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use App\Tournament;
use App\Http\helpers\UtilityHelper;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{
    public function create()
    {
        $getTournamentDetails = Tournament::where(['status'=> Tournament::ACTIVE_TOURNAMENTS])->get();
        return view('registration.create')->with('getTournamentDetails', $getTournamentDetails);;
    }

    public function store(Request $request)
    {
        $getData = Registration::get();
        $count = $getData->count();
        if($count < Registration::MAXIMUM_NO_OF_TEAMS) {

            $data = $_POST['form'];

            $register = Registration::create(
                [
                    'tournament_id' => $data[0]['value'],
                    'display_name' => $data[1]['value'],
                ]
            );

                return UtilityHelper::commonResponse(UtilityHelper::SUCCESS, '', 'The Team Has been Successfully Created.', 201);
            }else {

            return UtilityHelper::commonResponse(UtilityHelper::SUCCESS, '', 'Sorry! Maximum Number Of Teams Have been Already Registered.', 201);


        }
    }

    public function createTournament()
    {
        $getTournamentDetails = Tournament::where(['status'=> Tournament::ACTIVE_TOURNAMENTS])->get();

        return view('tournament.createTournament')->with('getTournamentDetails', $getTournamentDetails);
    }


    public function storeTournament(Request $request)
    {
            $register = Tournament::create(
                [
                    'display_name' => $_POST['tournament'],
                ]
            );
            if($register != null) {

                return UtilityHelper::commonResponse(UtilityHelper::SUCCESS, '', 'The Tournament Has been Successfully Created.', 201);
            }else{

                return UtilityHelper::commonResponse(UtilityHelper::SUCCESS, '', 'The Tournament creation Failed, Please Try Again!', 201);
            }

    }


}
?>