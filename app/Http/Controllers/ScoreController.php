<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use App\Tournament;
use App\TournamentDetails;
use App\ScoreDetails;
use App\Http\helpers\UtilityHelper;
use App\Http\Controllers\Controller;
Use Redirect;
use DB;

class ScoreController extends Controller
{

    public function viewScore(){

        $viewScore = DB::table('score_details')
            ->join('registrations', 'score_details.team_id', '=', 'registrations.id')
            ->join('tournaments', 'tournaments.id', '=', 'registrations.tournament_id')
            ->select('score_details.try','score_details.conversion', 'score_details.bonus'
                ,'score_details.total_score','score_details.status','score_details.win_count','tournaments.display_name as tournament','registrations.display_name as teamName')
            ->get();

        return view('tournament.viewScore')->with('viewScore',$viewScore);
    }

    public function storeScore(){

        $d= array();
        $random = array();
        $d['tournament_id'] = $_POST['tid'];
        $d['match_id'] = $_POST['tid'];
        $d['team_one_id'] = $_POST['team_id1'];
        $d['team_two_id'] = $_POST['team_id2'];
        $d['team_one_try'] = $_POST['team1_try'];
        $d['team_two_try'] = $_POST['team2_try'];
        $d['team_one_conversion'] = $_POST['team1_conv'];
        $d['team_two_conversion'] = $_POST['team2_conv'];
        $d['team_one_bonus'] = $_POST['bonus_team1'];
        $d['team_two_bonus'] = $_POST['bonus_team2'];

        $total_team1 = ($_POST['team1_try'] * 5) + ($_POST['team1_conv'] * 3) + ($_POST['bonus_team1'] * 6);
        $total_team2 = ($_POST['team2_try'] * 5) + ($_POST['team2_conv'] * 3) + ($_POST['bonus_team2'] * 6);


        $d['team_one_total'] = $total_team1;
        $d['team_two_total'] = $total_team2;

        if($total_team1 > $total_team2 ){
            $d['team_one_status'] = TournamentDetails::WON;
            $d['team_two_status'] = TournamentDetails::LOST;
        }
        elseif ($total_team1 < $total_team2){
            $d['team_one_status'] = TournamentDetails::LOST;
            $d['team_two_status'] = TournamentDetails::WON;
        }
        elseif ($total_team1 == $total_team2){
            if($_POST['bonus_team1'] > $_POST['bonus_team2']){
                $d['team_one_status'] = TournamentDetails::WON;
                $d['team_two_status'] = TournamentDetails::LOST;
            }
            elseif($_POST['bonus_team1'] < $_POST['bonus_team2']){
                $d['team_one_status'] = TournamentDetails::LOST;
                $d['team_two_status'] = TournamentDetails::WON;
            }
            elseif($_POST['bonus_team1'] == $_POST['bonus_team2']){
                if($_POST['team1_try'] > $_POST['team2_try'] ){
                    $d['team_one_status'] = TournamentDetails::WON;
                    $d['team_two_status'] = TournamentDetails::LOST;
                }
                elseif ($_POST['team1_try'] < $_POST['team2_try']){
                    $d['team_one_status'] = TournamentDetails::LOST;
                    $d['team_two_status'] = TournamentDetails::WON;
                }
                elseif ($_POST['team1_try'] == $_POST['team2_try']){
                    $strings = array(
                        0,
                        1,
                    );
                    shuffle($strings);
                    $d['team_one_status'] = $strings[0];
                    $d['team_two_status'] = $strings[1];
                }
            }
        }

        $d['status'] = TournamentDetails::MATCH_STOPPED;

            $createTournamentDetails = TournamentDetails::create(

                [
                    'team_one_id' => $d['team_one_id'],
                    'team_two_id' =>  $d['team_two_id'],
                    'match_id' =>  $d['team_two_try'],
                    'tournament_id' => $d['tournament_id'],
                    'team_one_try' =>$_POST['team1_try'],
                    'team_two_try' =>$_POST['team2_try'],
                    'team_one_conversion' =>$_POST['team1_conv'],
                    'team_two_conversion' =>$_POST['team1_conv'],
                    'team_one_bonus' =>$_POST['bonus_team1'],
                    'team_two_bonus'=>$_POST['bonus_team2'],
                    'team_one_total'=>$d['team_one_total'],
                    'team_two_total'=>$d['team_two_total'],
                    'team_one_status'=>$d['team_one_status'],
                    'team_two_status'=>$d['team_two_status'],
                    'status'=>$d['status']

                ]

            );

            $this->updateFinalScores($d);
        echo json_encode($d);

    }

    public function updateFinalScores($d)
    {
        $get_team_one = ScoreDetails::where(['team_id' => $d['team_one_id']],['tournament_id' => $d['tournament_id']])->get();

        if (!isset($get_team_one[0]->id)) {

            $createScore = ScoreDetails::create([

                    'tournament_id' => $d['tournament_id'],
                    'team_id' => $d['team_one_id'],
                    'try' => $d['team_one_try'],
                    'conversion' => $d['team_one_conversion'],
                    'bonus' => $d['team_one_bonus'],
                    'total_score' => $d['team_one_total'],
                    'win_count' => $d['team_one_status']
                ]
            );
        } else {

            $try = $d['team_one_try'] + $get_team_one[0]->try;
            $conversion = $d['team_one_conversion'] + $get_team_one[0]->conversion;
            $bonus = $d['team_one_bonus'] + $get_team_one[0]->bonus;
            $count = $d['team_one_status'] + $get_team_one[0]->win_count;
            $total = $d['team_one_total'] + $get_team_one[0]->total_score;

            $updateScore = ScoreDetails::where(['team_id' => $d['team_one_id']])->update(['try' => $try, 'conversion' => $conversion, 'bonus' => $bonus, 'win_count' => $count,'total_score' => $total]);

        }


        $get_team_two = ScoreDetails::where(['team_id'=>$d['team_two_id']],['tournament_id'=>$d['tournament_id']])->get();

        if(!isset($get_team_two[0]->id)){

            $createScore = ScoreDetails::create([

                    'tournament_id' =>$d['tournament_id'],
                    'team_id'       =>$d['team_two_id'],
                    'try'           =>$d['team_two_try'],
                    'conversion'    =>$d['team_two_conversion'],
                    'bonus'         =>$d['team_two_bonus'],
                    'total_score'   =>$d['team_two_total'],
                    'win_count'     =>$d['team_two_status']
                ]
            );
        }else{

            $try = $d['team_two_try']+$get_team_two[0]->try;
            $conversion = $d['team_two_conversion'] + $get_team_two[0]->conversion;
            $bonus = $d['team_two_bonus'] + $get_team_two[0]->bonus;
            $count = $d['team_two_status'] + $get_team_two[0]->win_count;
            $total = $d['team_two_total'] + $get_team_two[0]->total_score;

            $updateScore = ScoreDetails::where(['team_id'=>$d['team_two_id']])->update(['try' => $try, 'conversion' => $conversion, 'bonus' => $bonus, 'win_count' => $count,'total_score' => $total]);

        }


    }


    public function setWinner(){

        $win_by_bonus = array();

        $win_by_total = array();
        $k = array();

        $max_win_count = ScoreDetails::max('win_count');

        $team2 = ScoreDetails::where(['win_count'=>$max_win_count])->get();

        foreach($team2 as $data){

            $team3[] = array('id' => $data->id, 'tournament_id' => $data->tournament_id, 'team_id' => $data->team_id ,
                'try' => $data->try,'conversion' => $data->conversion,'bonus' => $data->bonus,
                'total_score' => $data->total_score,'win_count' => $data->win_count);

        }

        if(count($team3) > 1){
            $z= max(array_column($team3, 'bonus'));
            foreach($team3 as $team){
                if($team['bonus']==$z){
                    array_push($win_by_bonus,$team);
                }
            }
            if(count($team3) > 1){
                $y= max(array_column($win_by_bonus, 'total_score'));
                foreach($win_by_bonus as $team){
                    if($team['total_score']==$y){
                        array_push($win_by_total,$team);

                    }
                }
                foreach($win_by_total as $team){
                    array_push($k,$team['team_id']);
                }
                $d['wining_team '] = $k;
            }
            else{
                $d['wining_team '][0] = $win_by_bonus[0]['team_id'];
            }


        }
        else{
            $d['wining_team '][0] = $team3[0]['team_id'];
        }

        $winner = $d['wining_team '][0];

        DB::table('Score_details')->update(array('status' => TournamentDetails::LOST));

        foreach($d['wining_team '] as $winner){

            $updateWinner = ScoreDetails::where(['team_id'=>$winner])->update(['status' => TournamentDetails::WON ]);

        }


        return $this->viewScore();

    }


}
?>