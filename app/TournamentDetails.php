<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class TournamentDetails extends Authenticatable
{
    use Notifiable;
    const MATCH_STARTED = 1;
    const MATCH_STOPPED= 0;
    const LOST = 0;
    const WON = 1;



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_id' , 'match_id','team_one_id','team_two_id','team_one_try',
        'team_two_try','team_one_conversion','team_two_conversion','team_one_bonus',
        'team_two_bonus','team_one_total','team_two_total','team_one_status','team_two_status',
        'status','created_at','updated_at','deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];


}